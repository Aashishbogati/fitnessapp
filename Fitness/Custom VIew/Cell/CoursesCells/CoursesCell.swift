//
//  CoursesCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/24/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class CoursesCell: BaseCell {
    
    var data : Coursesdata? {
        didSet {
            if let imageName = data?.thumb_image {
                thumbImage.image = UIImage(named: imageName)
            }
            if let title = data?.title {
                titleLabel.text = title
            }
            
            if let hours = data?.hours {
                hoursLabel.text = hours
            }
        }
    }
    
    @IBOutlet weak var thumbImage: CornorRadiusImageView!
    @IBOutlet weak var hoursLabel: WhiteTitleLabel!
    @IBOutlet weak var titleLabel: WhiteHeadingLabel!
}
