//
//  ProfilePhotoCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/20/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ProfilePhotoCell: BaseCell {
    var photo : Photos? {
        didSet {
            if let imageName = photo?.imageName {
                photos.image = UIImage(named: imageName)
            }
        }
    }
    @IBOutlet weak var photos: CornorRadiusImageView!
}
