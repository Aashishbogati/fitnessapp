//
//  ProfileTrainingCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/20/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ProfileTrainingCell: BaseCell {
    
    var training : Training? {
        didSet {
            
            if let thumbImageName = training?.thumb_image {
                thumbImage.image = UIImage(named: thumbImageName)
            }
            
            if let trainingName = training?.name {
                trainingNameLabel.text = trainingName
            }
            
            if let time = training?.time {
                timeLabel.text = time
            }
        }
    }
    
    @IBOutlet weak var thumbImage: CornorRadiusImageView!
    @IBOutlet weak var trainingNameLabel: SmallTitleLabel!
    @IBOutlet weak var timeLabel: SecondaryLabel!
    
    override class func awakeFromNib() {
        
    }
    
    
}
