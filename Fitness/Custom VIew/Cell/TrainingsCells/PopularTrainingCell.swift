//
//  PopularTrainingCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/23/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class PopularTrainingCell: BaseCell {
    
    var popdata : PouplarTraining? {
        didSet {
            if let thumbImageName = popdata?.thumb_image {
                thumbImage.image = UIImage(named: thumbImageName)
            }
            
            if let titleLabelName = popdata?.name {
                titleLabe.text = titleLabelName
            }
            
            if let catName = popdata?.catName {
                catLabel.text = catName
            }
        }
    }
    
    @IBOutlet weak var thumbImage: CornorRadiusImageView!
    @IBOutlet weak var titleLabe: TitleLabel!
    @IBOutlet weak var catLabel: SecondaryLabel!
    @IBOutlet weak var newBtn: UIButton!
    override func awakeFromNib() {
        configNewButton()
    }
    
    //MARK:- config new button
    func configNewButton() {
        newBtn.layer.cornerRadius = 9
    }
    
}
