//
//  HeroSliderCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/23/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class HeroSliderCell: BaseCell {
    var data : HeroSliderImage? {
        didSet {
            if let imageName = data?.thumb_image {
                thumbImage.image = UIImage(named: imageName)
            }
        }
    }
    @IBOutlet weak var thumbImage: UIImageView!
}
