//
//  CornorRadiusImageView.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/20/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class CornorRadiusImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configImageView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configImageView()
    }
    
    //MARK:- config image view
    func configImageView() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }

}
