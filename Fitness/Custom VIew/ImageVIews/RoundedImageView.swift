//
//  RoundedImageView.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/19/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configImageView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configImageView()
    }
    
    //MARK:- config image view
    func configImageView() {
        self.layer.cornerRadius = frame.height / 2
        self.contentMode = .scaleAspectFill
    }

}
