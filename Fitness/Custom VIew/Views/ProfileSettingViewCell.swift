//
//  ProfileSettingViewCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/24/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ProfileSettingViewCell: BaseCell {
    
    var settings : Settings? {
        didSet {
    
            if let settingName = settings?.name {
                settingLabel.text = settingName
            }
            
        }
    }

    let settingLabel : UILabel = {
        let label = UILabel()
        label.text = "Settings"
        label.font = .systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    

    
    override func configViews() {
        
        addSubview(settingLabel)
        
        NSLayoutConstraint.activate([
            
            settingLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            settingLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 20),
            settingLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: 8),
        ])

    }
}
