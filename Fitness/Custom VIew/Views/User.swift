//
//  User.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/24/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import Foundation

struct User {
    var name : String?
    var profile_image : String?
    var followersCount : String?
    var followingCount : String?
    var userStatus : String?
    var hours : String?
}


struct Training {
    let thumb_image : String?
    let name : String?
    let time : String?
}

struct Photos {
    let imageName : String?
}

struct PouplarTraining {
    let thumb_image : String?
    let name : String?
    let catName : String?
}

struct HeroSliderImage {
    let thumb_image : String?
}

struct Coursesdata {
    let thumb_image : String?
    let title : String?
    let hours : String?
}
