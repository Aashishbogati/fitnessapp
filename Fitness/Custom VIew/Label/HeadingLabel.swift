//
//  HeadingLabel.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/20/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class HeadingLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configHeadingLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configHeadingLabel()
    }
    
    //MARK:- config heading label
    func configHeadingLabel() {
        self.textColor = .black
        self.font = .systemFont(ofSize: 25, weight: UIFont.Weight(rawValue: 800))
    }
}
