//
//  TitleLabel.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/19/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class TitleLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configTitleLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configTitleLabel()
    }
    

    //MARK:- config title label
    func configTitleLabel() {
        self.textColor = .black
        self.font = .systemFont(ofSize: 18, weight: UIFont.Weight(rawValue: 800))
    }

}
