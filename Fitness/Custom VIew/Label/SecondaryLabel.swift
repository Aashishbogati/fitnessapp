//
//  SecondaryLabel.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/19/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class SecondaryLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configSecondaryLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configSecondaryLabel()
    }
    
    //MARK:- config secondary label
    func configSecondaryLabel() {
        self.font = .boldSystemFont(ofSize: 14)
        self.textColor = ColorCompatibility.secondaryLabel
    }

}
