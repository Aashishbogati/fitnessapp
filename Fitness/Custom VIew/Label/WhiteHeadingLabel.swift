//
//  WhiteHeadingLabel.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/23/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class WhiteHeadingLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configHeadingLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configHeadingLabel()
    }
    
    //MARK:- config heading label
    func configHeadingLabel() {
        self.textColor = .white
        self.font =  .boldSystemFont(ofSize: 30)
    }

}
