//
//  DefaultButton.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/21/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class DefaultButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configButton()
    }
    
    
    //MARK:- config button
    func configButton() {
        self.backgroundColor = ColorCompatibility.label
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = .boldSystemFont(ofSize: 14)
        self.layer.cornerRadius = 9
        self.clipsToBounds = true
    }
}
