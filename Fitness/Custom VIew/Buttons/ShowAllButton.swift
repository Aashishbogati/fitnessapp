//
//  ShowAllButton.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/20/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ShowAllButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configShowAllBtn()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configShowAllBtn()
    }
    
    //MARK:- config show all btn
    func configShowAllBtn() {
        self.titleLabel?.font = .boldSystemFont(ofSize: 13)
        self.setTitleColor(ColorCompatibility.secondaryLabel, for: .normal)
    }

}
