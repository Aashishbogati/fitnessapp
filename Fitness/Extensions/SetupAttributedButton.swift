//
//  SetupAttributedButton.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/22/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class SetupAttributedButton {
    static var shared = SetupAttributedButton()
    
    func setupButton(firstString: String?,secondString: String?,buttonName:UIButton?) {
        let attributedText = NSMutableAttributedString(string: firstString!, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 16),NSAttributedString.Key.foregroundColor : UIColor.init(white: 0, alpha: 0.65)])
        
        let attributedSubString = NSMutableAttributedString(string: secondString!, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 18),NSAttributedString.Key.foregroundColor : UIColor.black])
        
        attributedText.append(attributedSubString)
        buttonName?.setAttributedTitle(attributedText, for: .normal)
        
    }

}
