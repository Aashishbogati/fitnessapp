//
//  BaseCell.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/20/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        configViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configViews()
    }
    
    //MARK:- config views
    func configViews() {
        
    }
}
