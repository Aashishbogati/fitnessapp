//
//  ViewController.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/19/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    
    
    var user : User? {
        didSet {
            if let name = user?.name {
                userNameLabel.text = name
            }
            
            if let profileImagename = user?.profile_image {
                profileImage.image = UIImage(named: profileImagename)
            }
            
            if let followerCount = user?.followersCount {
                followersLabel.text = followerCount
            }
            
            if let followingCount = user?.followingCount {
                followingLabel.text = followingCount
            }
            
            if let userStatus = user?.userStatus {
                userStatusLabel.text = userStatus
            }
            
            if let hours = user?.hours {
                hoursLabel.text = hours
            }
        }
    }
    
    //MARK:- training data
    let trainings : [Training] = {
        let bulgaraianSquat = Training(thumb_image: "buls", name: "Bulgarian Squat", time: "2 days ago")
        let powerLifting = Training(thumb_image: "pw", name: "Power Lifting", time: "1 days ago")
        let pushUp = Training(thumb_image: "pushup", name: "Push-up", time: "3 days ago")
        let keepFit = Training(thumb_image: "keepfit", name: "Keep fit", time: "4 days ago")
        return [bulgaraianSquat,powerLifting,pushUp,keepFit]
    }()
    
    //MARK:- photo collection data
    let photos : [Photos] = {
        let image1 = Photos(imageName: "image1")
        let image2 = Photos(imageName: "image2")
        let image3 = Photos(imageName: "image3")
        let image4 = Photos(imageName: "buls")
        let image5 = Photos(imageName: "pw")
        let image6 = Photos(imageName: "keepfit")
        return [image1,image2,image3,image4,image5,image6]
    }()
    
    //MARK:- profile iboutlet
    @IBOutlet weak var profileImage: RoundedImageView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var followingLabel: SecondaryLabel! {
        didSet {
            followingLabel.text = "following"
        }
    }
    @IBOutlet weak var followersLabel: SecondaryLabel! {
        didSet {
            followersLabel.text = "followers"
        }
    }
    @IBOutlet weak var followingCountLabel: TitleLabel!
    @IBOutlet weak var follwersCountLabel: TitleLabel!
    @IBOutlet weak var profileViewContainer: UIView!
    @IBOutlet weak var userNameLabel: TitleLabel!
    @IBOutlet weak var userStatusLabel: UILabel! {
        didSet {
            userStatusLabel.font = .boldSystemFont(ofSize: 14)
        }
    }
    @IBOutlet weak var hoursLabel: UILabel! {
        didSet {
            hoursLabel.font = .boldSystemFont(ofSize: 14)
        }
    }
    
    @IBOutlet weak var statisticsHeaderLabel: HeadingLabel! {
        didSet {
            statisticsHeaderLabel.text = "Statistics"
        }
    }
    @IBOutlet weak var weekEndLabel: SecondaryLabel! {
        didSet {
            weekEndLabel.text = "This week"
        }
    }
    @IBOutlet weak var showMoreBtn: ShowAllButton! {
        didSet {
            showMoreBtn.setTitle("Show all", for: .normal)
        }
    }
    
    @IBOutlet weak var chartContainer: UIView!
    @IBOutlet weak var trainingCollectionView:
    UICollectionView!
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configNavigationBar()
        configCharContainer()
        configTrainingCollectionView()
        configPhotoCollectionView()
        self.scrollView.delegate = self
        setUserData()
    }
    
    //MARK:- user model data
    func setUserData() {
        //MARK: User
        let user1 = User(name: "Carly Jones", profile_image: "profile_image", followersCount: "1,350", followingCount: "300", userStatus: "Beginner", hours: "120hrs")
        self.user = user1
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == mainScrollView {
            
            if(scrollView.convert(view.frame.origin, to: self.view).y <= 60) {
              configNavigationBarTitle()
            } else {
                navigationItem.titleView = UILabel()
            }
        }
        
    }
    
    //MARK:- functions
    
    func configNavigationBarTitle() {
        let titleLabel = UILabel(frame: .init(x: 0, y: 0, width: 34, height: 34))
        titleLabel.text = userNameLabel.text
        titleLabel.font = .systemFont(ofSize: 18, weight: UIFont.Weight(rawValue: 800))
        navigationItem.titleView = titleLabel
    }
    

    

    func configCharContainer() {
        chartContainer.layer.cornerRadius = 9
    }
    
   
    func configTrainingCollectionView() {
        if let layout = trainingCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        
        trainingCollectionView.delegate = self
        trainingCollectionView.dataSource = self
    }
    
    //MARK:- config training collection view
    func configPhotoCollectionView() {
        photoCollectionView.delegate = self
        photoCollectionView.dataSource = self
    }
   
    
    func configNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = Color.hexStringToUIColor(hex: "F8F8FA")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil,for: .default)
        //MARK:- navigation bar right button
        configNavigationBarRightButton()
    }
    
   
    func configNavigationBarRightButton() {
        let moreIconbtn = UIButton(frame: .init(x: 0, y: 0, width: 20, height: 20))
        moreIconbtn.setImage(UIImage(named: "more"), for: .normal)
        moreIconbtn.tintColor = .black
        moreIconbtn.addTarget(self, action: #selector(showMoreSettings), for: .touchUpInside)
        moreIconbtn.imageEdgeInsets = .init(top: 4, left: 4, bottom: 4, right: 4)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreIconbtn)
    }
    
   
    lazy var settingView : ProfileSettingViews = {
        let sv = ProfileSettingViews()
        return sv
    }()
    
    //MARK:- show more settings
    @objc func showMoreSettings() {
        settingView.showSettings()
    }
    
   

}


extension ProfileVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //MARK:- training collection view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.photoCollectionView {
            return photos.count
        }
        return trainings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.photoCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePhotoCell", for: indexPath) as! ProfilePhotoCell
            cell.photo = photos[indexPath.row]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileTrainingCell", for: indexPath) as! ProfileTrainingCell
        cell.training = trainings[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == photoCollectionView {
            return .init(width: (collectionView.frame.width - 20) / 3, height: (collectionView.frame.width - 20) / 3)
        }
        return .init(width: collectionView.frame.width / 2, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    
}
