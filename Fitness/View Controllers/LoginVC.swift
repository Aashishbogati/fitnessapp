//
//  LoginVC.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/22/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var signUpbutton: UIButton!
    @IBAction func loginButtonAction(_ sender: DefaultButton) {
        let tabBarVC = UIStoryboard(name: "Tabbar", bundle: nil).instantiateViewController(withIdentifier: "TabbarVC") as! TabbarVC
        self.view.window?.rootViewController = tabBarVC
    }

    @IBAction func SignupButtonAction(_ sender: UIButton) {
        let signupVC = UIStoryboard(name: "Signup", bundle: nil).instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        navigationController?.pushViewController(signupVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = Color.hexStringToUIColor(hex: "F8F8FA")
        configNavigationBar()
        setupSignUpButton()
    }
    
    
    //MARK:- functions
    func configNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = Color.hexStringToUIColor(hex: "F8F8FA")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil,for: .default)
        self.navigationController?.navigationBar.tintColor = .black
    }
    
    func setupSignUpButton() {
        SetupAttributedButton.shared.setupButton(firstString: "Don't have an account? ", secondString: "Sign Up!", buttonName: signUpbutton)
    }
    
    
    

}
