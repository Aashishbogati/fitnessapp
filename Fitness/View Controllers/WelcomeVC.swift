//
//  MainVC.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/21/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBAction func loginButtonAction(_ sender: UIButton) {
        let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func startTrainingBtn(_ sender: Any) {
        let signUpVC = UIStoryboard(name: "Signup", bundle: nil).instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupLoginButton()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    //MARK:- setup login button
    func setupLoginButton() {
        SetupAttributedButton.shared.setupButton(firstString: "Already have account? ", secondString: "Login", buttonName: loginButton)
    }
    
    

    
}
