//
//  HomeVC.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/23/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class TrainingVC: UIViewController {

    @IBOutlet weak var getNowBtn: UIButton!
    @IBOutlet weak var heroSliderCollectionView: UICollectionView!
    @IBOutlet weak var popularTrainingCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    let popTraining : [PouplarTraining] = {
        let training1 = PouplarTraining(thumb_image: "pw",name: "Power Lifting",catName:"gym taining")
        let training3 = PouplarTraining(thumb_image: "image2",name: "Push-up",catName:"home training")
        let training2 = PouplarTraining(thumb_image: "image1",name: "Legs and Abs",catName:"gym training")
        let training4 = PouplarTraining(thumb_image: "pw",name: "Power Lifting",catName:"Gym taining")
        return [training1,training2,training3,training4]
    }()
    
    let heroImages : [HeroSliderImage] = {
        let hero1 = HeroSliderImage(thumb_image: "h1")
        let hero2 = HeroSliderImage(thumb_image: "h2")
        let hero3 = HeroSliderImage(thumb_image: "h3")
        return [hero2,hero1,hero3]
    }()
    
    var timer = Timer()
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configNavigationBar()
        configGetNowBtn()
        pageControl.numberOfPages = 3
        pageControl.currentPage = 0
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
        
    }
    

    func configGetNowBtn() {
        getNowBtn.layer.cornerRadius = 9
        getNowBtn.clipsToBounds = true
    }
    
    @objc func changeImage() {
        if counter < 3 {
            let index = IndexPath.init(item: counter, section: 0)
            self.heroSliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageControl.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.heroSliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageControl.currentPage = counter
            counter = 1
        }
    }
    
    

    func configNavigationBar() {
        //MARK:- config navigation bar
        self.navigationController?.navigationBar.barTintColor = Color.hexStringToUIColor(hex: "F8F8FA")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil,for: .default)
        
        configNavigationBarRightButton()
        configNavigationBarTitle()
        configNavigationLeftBarButton()
        
        configHeroSliderCollectionView()
        configPopularTrainingSection()
    }
    
    func configNavigationBarTitle() {
        let titleLabel = UILabel(frame: .init(x: 0, y: 0, width: 34, height: 34))
        titleLabel.text = "Training"
        titleLabel.font = .systemFont(ofSize: 18, weight: UIFont.Weight(rawValue: 800))
        navigationItem.titleView = titleLabel
    }
    
    func configNavigationLeftBarButton() {
        let bellIconBtn = UIButton(frame: .init(x: 0, y: 0, width: 20, height: 20))
        bellIconBtn.setImage(UIImage(named: "bell"), for: .normal)
        bellIconBtn.tintColor = .black
        bellIconBtn.imageEdgeInsets = .init(top: 4, left: 4, bottom: 4, right: 4)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: bellIconBtn)
    }
    
    func configNavigationBarRightButton() {
        let moreIconbtn = UIButton(frame: .init(x: 0, y: 0, width: 20, height: 20))
        moreIconbtn.setImage(UIImage(named: "more"), for: .normal)
        moreIconbtn.tintColor = .black
        moreIconbtn.imageEdgeInsets = .init(top: 4, left: 4, bottom: 4, right: 4)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreIconbtn)
    }
    
    

    func configHeroSliderCollectionView() {
        if let layout = heroSliderCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
        }
        self.heroSliderCollectionView.showsHorizontalScrollIndicator = false
        self.heroSliderCollectionView.isPagingEnabled = true
        self.heroSliderCollectionView.delegate = self
        self.heroSliderCollectionView.dataSource = self
    }
    
    func configPopularTrainingSection() {
        if let layout = popularTrainingCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
           
        }
        self.popularTrainingCollectionView.showsHorizontalScrollIndicator = false
        self.popularTrainingCollectionView.delegate = self
        self.popularTrainingCollectionView.dataSource = self
    }

}


extension TrainingVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == popularTrainingCollectionView {
            return popTraining.count
        }
        
        return heroImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == popularTrainingCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularTrainingCell", for: indexPath) as! PopularTrainingCell
            cell.popdata = popTraining[indexPath.row]
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroSliderCell", for: indexPath) as! HeroSliderCell
        cell.data = heroImages[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == popularTrainingCollectionView {
            return .init(width: collectionView.frame.width / 1.5, height: 200)
        }
        
        return .init(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
}
