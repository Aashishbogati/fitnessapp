//
//  SignupVC.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/22/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    @IBOutlet weak var signupHeadingLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    

    @IBAction func LoginButtonAction(_ sender: UIButton) {
        let loginVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = Color.hexStringToUIColor(hex: "F8F8FA")
        configNavigationBar()
        configSignupHeadingLabel()
        SetupAttributedButton.shared.setupButton(firstString: "Already have account?", secondString: " Login", buttonName: loginButton)
    }
    
    //MARK:- functions
    func configNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = Color.hexStringToUIColor(hex: "F8F8FA")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil,for: .default)
        self.navigationController?.navigationBar.tintColor = .black
    }
    
   
    func configSignupHeadingLabel() {
        let attributedText = NSMutableAttributedString(string: "Sign up to get \n", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: UIFont.Weight(rawValue: 400))])
        
        let attributedSubText = NSMutableAttributedString(string: "5 Premium\nCourses", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 40, weight: UIFont.Weight(rawValue: 500)),NSAttributedString.Key.foregroundColor : UIColor.systemOrange])
        
        attributedText.append(attributedSubText)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
         attributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedText.length))
        
        signupHeadingLabel.attributedText = attributedText
        signupHeadingLabel.numberOfLines = 0
        
    }
    
  


}
