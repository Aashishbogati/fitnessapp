//
//  TabbarVC.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/23/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class TabbarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configTabBarController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.selectedIndex = 1
    }
    

    func configTabBarController() {
  
        tabBar.tintColor = .black
        

        let trainingVC = UINavigationController(rootViewController: UIStoryboard(name: "Training", bundle: nil).instantiateViewController(withIdentifier: "TrainingVC") as! TrainingVC)
        trainingVC.tabBarItem.title = "Training"
        trainingVC.tabBarItem.image = UIImage(named: "Training")
        
        let coursesVC = UINavigationController(rootViewController: UIStoryboard(name: "Courses", bundle: nil).instantiateViewController(withIdentifier: "CoursesVC") as! CoursesVC)
        coursesVC.tabBarItem.title = "Courses"
        coursesVC.tabBarItem.image = UIImage(named: "film")
 
        
        let profileVC = UINavigationController(rootViewController: UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC)
        profileVC.tabBarItem.title = "Profile"
        profileVC.tabBarItem.image = UIImage(named: "user")

        viewControllers = [trainingVC,coursesVC,profileVC]
    }
  

}
