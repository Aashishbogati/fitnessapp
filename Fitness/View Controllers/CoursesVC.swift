//
//  CoursesVC.swift
//  Fitness
//
//  Created by Aashish Bogati on 3/24/20.
//  Copyright © 2020 Aashish Bogati. All rights reserved.
//

import UIKit

class CoursesVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let coursesData : [Coursesdata] = {
        let course1 = Coursesdata(thumb_image: "image1",title: "Flow Yoga", hours: "120 hours")
        let course2 = Coursesdata(thumb_image: "pw",title: "Power Lifting", hours: "10 hours")
        let course3 = Coursesdata(thumb_image: "h2",title: "Bulgarian Squat", hours: "12 hours")
        let course4 = Coursesdata(thumb_image: "image3",title: "Flow Yoga", hours: "120 hours")
        let course5 = Coursesdata(thumb_image: "keepfit",title: "Keep Fit", hours: "140 hours")
        return [course1,course2,course3,course4,course5]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configNavigationBar()
        configCollectionView()
    }
    
    func configCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func configNavigationBar() {
        //MARK:- config navigation bar
        self.navigationController?.navigationBar.barTintColor = Color.hexStringToUIColor(hex: "F8F8FA")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(nil,for: .default)
        
        configNavigationBarRightButton()
        configNavigationBarTitle()
    }
    
    func configNavigationBarTitle() {
        let titleLabel = UILabel(frame: .init(x: 0, y: 0, width: 34, height: 34))
        titleLabel.text = "All Courses"
        titleLabel.font = .systemFont(ofSize: 18, weight: UIFont.Weight(rawValue: 800))
        navigationItem.titleView = titleLabel
    }

    
    func configNavigationBarRightButton() {
        let moreIconbtn = UIButton(frame: .init(x: 0, y: 0, width: 20, height: 20))
        moreIconbtn.setImage(UIImage(named: "more"), for: .normal)
        moreIconbtn.tintColor = .black
        moreIconbtn.imageEdgeInsets = .init(top: 4, left: 4, bottom: 4, right: 4)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: moreIconbtn)
    }
}

extension CoursesVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coursesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoursesCell", for: indexPath) as! CoursesCell
        cell.data = coursesData[indexPath.row]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width, height: 200)
    }
    
    

}
